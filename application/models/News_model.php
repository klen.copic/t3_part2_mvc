<?php
//https://www.studenti.famnit.upr.si/phpmyadmin/
class News_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_news($slug = FALSE)
    {
    	if ($slug === FALSE)
        {
                $query = $this->db->get('news');
                return $query->result_array();
        }
        
    	$query = $this->db->get_where('news', array('slug' => $slug));
    	return $query->row_array();

    }

    public function delete($slug){
        $this->db->where("slug", $slug);
        return $this->db->delete("news");
    }

    public function save($slug){
        $data["title"] = $this->input->post("title");
        $data["text"] = $this->input->post("text");

        $this->db->set($data);
        $this->db->where("slug", $slug);
        $this->db->update("news");

    }

    public function save_news(){

        $slug = url_title($this->input->post("title"), "dash", TRUE);

        $data = array(
            'title' => $this->input->post("title"),
            'slug' => $slug,
            'text' => $this->input->post("text")
        );
        $this->db->insert("news", $data);
    }
}